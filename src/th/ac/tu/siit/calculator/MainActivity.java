package th.ac.tu.siit.calculator;

import android.os.Bundle;
import android.R.integer;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
	implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button b0 = (Button)findViewById(R.id.num0);
        b0.setOnClickListener(this);
        Button b1 = (Button)findViewById(R.id.num1);
        b1.setOnClickListener(this);
        Button b2 = (Button)findViewById(R.id.num2);
        b2.setOnClickListener(this);
        Button b3 = (Button)findViewById(R.id.num3);
        b3.setOnClickListener(this);
        Button b4 = (Button)findViewById(R.id.num4);
        b4.setOnClickListener(this);
        Button b5 = (Button)findViewById(R.id.num5);
        b5.setOnClickListener(this);
        Button b6 = (Button)findViewById(R.id.num6);
        b6.setOnClickListener(this);
        Button b7 = (Button)findViewById(R.id.num7);
        b7.setOnClickListener(this);
        Button b8 = (Button)findViewById(R.id.num8);
        b8.setOnClickListener(this);
        Button b9 = (Button)findViewById(R.id.num9);
        b9.setOnClickListener(this);
        
        ((Button)findViewById(R.id.add)).setOnClickListener(this);
        ((Button)findViewById(R.id.sub)).setOnClickListener(this);
        ((Button)findViewById(R.id.mul)).setOnClickListener(this);
        ((Button)findViewById(R.id.div)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.ac)).setOnClickListener(this);
        ((Button)findViewById(R.id.bs)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.dot)).setOnClickListener(this);
        ((Button)findViewById(R.id.equ)).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    String display = "0";
    
    int state = 0;
    // state==0 initial
    // state==1 input one operand
    // state==2 output generated
    
    int lastButton = 0;
    // lastButton==0 numeric
    // lastButton==1 operator
    
    double value1, value2, result;
    int operator;
    
    @Override
    public void onClick(View v) {
    	int id = v.getId();
    	TextView output = (TextView)findViewById(R.id.output);
		TextView operatorTV = (TextView)findViewById(R.id.operator);

    	
    	switch(id){
    	case R.id.num0:
    	case R.id.num1:
    	case R.id.num2:
    	case R.id.num3:
    	case R.id.num4:
    	case R.id.num5:
    	case R.id.num6:
    	case R.id.num7:
    	case R.id.num8:
    	case R.id.num9:
    	case R.id.dot:
    		if(display.equals("0") || lastButton == 1){
    			display = "";
    		}
    		display += ((Button)v).getText().toString();
    		output.setText(display);
    		
    		break;
    	case R.id.bs:
    		if(display.equals(0) || display.length()==1){
    			display = "0";
    			output.setText(display);
    			lastButton = 0;
    		}
    		else{
    			display = display.substring(0, display.length()-1);
    			output.setText(display);
    		}
    		break;
    		
    	case R.id.ac:
    		display="0";
    		lastButton = 0;
    		state = 0;
    		output.setText(display);
    		break;
    
    	case R.id.add:
    	case R.id.sub:
    	case R.id.mul:
    	case R.id.div:
    		//have no input. Input value 1
    		if (state == 0) {
				value1 = Double.parseDouble(display);
				operator = id;
				state = 1;
				displayOperator(operatorTV, operator);
			}
    		//input value 2
    		else if(state == 1){
    			value2 = Double.parseDouble(display);
				if (operator == R.id.add) {
					value1 = value1 + value2;
				}
				else if (operator == R.id.sub) {
					value1 = value1 - value2;
				}
				else if (operator == R.id.mul) {
					value1 = value1 * value2;
				}
				else if (operator == R.id.div) {
					value1 = value1 / value2;
				}
				
				output.setText(String.valueOf(value1));
				operator = id;
				displayOperator(operatorTV, operator);
				state = 1;
    		}
    		
    		else if(state == 2){
    			value1 = Double.parseDouble(display);
				operator = id;
				displayOperator(operatorTV, operator);
				state = 1;
    		}
    		lastButton = 1;
    	break;

    	case R.id.equ:
    	{
    		value2 = Double.parseDouble(display);
			if (operator == R.id.add) {
				value1 = value1 + value2;
			}
			else if (operator == R.id.sub) {
				value1 = value1 - value2;
			}
			else if (operator == R.id.mul) {
				value1 = value1 * value2;
			}
			else if (operator == R.id.div) {
				value1 = value1 / value2;
			}
			
			output.setText(String.valueOf(value1));
			operator = id;
			displayOperator(operatorTV, operator);
			state = 1;
    	}
    		
    		break;
    	}
    }
    void displayOperator(TextView t, int operator) {
    	
    	switch(operator){
    	case R.id.add:
    		t.setText("+");
    		break;
    	case R.id.sub:
    		t.setText("-");
    		break;
    	case R.id.mul:
    		t.setText("x");
    		break;
    	case R.id.div:
    		t.setText("/");
    		break;
    	case R.id.equ:
    		t.setText("=");
    		break;
    	}
    }
	
}


